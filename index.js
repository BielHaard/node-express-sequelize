
const express = require("express");
const app = express();


app.get("/", (req, res) => {
    res.sendFile(__dirname + '/design/index.html');
});

app.get("/julio", (req, res) => {
    res.send('Olá eu sou o Júlio!');
});

app.get("/sobre", (req, res) => {
    res.send('Essa é minha página sobre');
});


// Lembrar que só podemos adicionar um SEND por requisição!
// Exeplo abaixo de recuperação de Parametros.
app.get("/ola/:nome/:sobrenome", (req, res) => {
    res.send("<h1> Meu nome é: " + req.params.nome + "</h1>" + "<h2> E meu sobrenome é: " + req.params.sobrenome + "</h2>");
});


// o Listen do Express sempre tem que ser na ultima linha do código.
app.listen(8190,  () => { 
    console.log('Sua aplicação está rodando na porta (8190)!')
});